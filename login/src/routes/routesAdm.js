import React, { useContext } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Context } from '../Context/AuthContext';
import { Login } from '../pages/Login';
import Dashboard from '../pages/Dashboard';
import Perfil from '../pages/Perfil';
import baseDashboard from '../containers/dashboard';
import { Listar } from '../pages/User/Listar';
import { Visualizar } from '../pages/User/Visualizar';
import { Cadastrar } from '../pages/User/Cadastrar';
import { Editar } from '../pages/User/Editar';

import { ListarCurso } from '../pages/Course/Listar';
import { VisualizarCurso } from '../pages/Course/Visualizar';
import { CadastrarCurso } from '../pages/Course/Cadastrar';
import { EditarCurso } from '../pages/Course/Editar';

import { ListarRl } from '../pages/Role/Listar';
import { CadastrarRl } from '../pages/Role/Cadastrar';
import { EditarRl } from '../pages/Role/Editar';
//import { VisualizarRl } from '../pages/Role/Visualizar';

import { ListarInstrutores } from '../pages/Instructor/Listar';
import { VisualizarInstrutor } from '../pages/Instructor/Visualizar';

import { ListarTurmas } from '../pages/Classe/Listar';

import { ListarAlunos } from '../pages/Student/Listar';
import { VisualizarAluno } from '../pages/Student/Visualizar';


export default function RoutesAdm() {

    function CustomRoute({ isPrivate, ...rest }) {
        const { authenticated } = useContext(Context);

        if (isPrivate && !authenticated) {
            return <Redirect to="/" />
        }

        return <Route {...rest} />
    }

    return (
        <Switch>
            <CustomRoute exact path="/" component={Login} />
            <CustomRoute isPrivate exact path="/dashboard" component={baseDashboard(Dashboard)} />
            <CustomRoute isPrivate exact path="/perfil" component={baseDashboard(Perfil)} />
            <CustomRoute isPrivate exact path="/listar" component={baseDashboard(Listar)} />
            <CustomRoute isPrivate exact path="/visualizar/:id" component={baseDashboard(Visualizar)} />
            <CustomRoute isPrivate exact path="/cadastrar" component={baseDashboard(Cadastrar)} />
            <CustomRoute isPrivate exact path="/editar/:id" component={baseDashboard(Editar)} />

            <CustomRoute isPrivate exact path="/listar-cursos" component={baseDashboard(ListarCurso)} />
            <CustomRoute isPrivate exact path="/visualizar-curso/:id" component={baseDashboard(VisualizarCurso)} />
            <CustomRoute isPrivate exact path="/cadastrar-curso" component={baseDashboard(CadastrarCurso)} />
            <CustomRoute isPrivate exact path="/editar-curso/:id" component={baseDashboard(EditarCurso)} />

            <CustomRoute isPrivate exact path="/listarRl" component={baseDashboard(ListarRl)} />
            <CustomRoute isPrivate exact path="/cadastrarRl" component={baseDashboard(CadastrarRl)} />
            <CustomRoute isPrivate exact path="/editarRl/:id" component={baseDashboard(EditarRl)} />
            <CustomRoute isPrivate exact path="/visualizarRl/:id" component={baseDashboard(ListarRl)} />

            <CustomRoute isPrivate exact path="/listar-instrutores" component={baseDashboard(ListarInstrutores)} />
            <CustomRoute isPrivate exact path="/visualizar-instrutor/:id" component={baseDashboard(VisualizarInstrutor)} />

            <CustomRoute isPrivate exact path="/listar-turmas" component={baseDashboard(ListarTurmas)} />

            <CustomRoute isPrivate exact path="/listar-participantes" component={baseDashboard(ListarAlunos)} />
            <CustomRoute isPrivate exact path="/visualizar-participante/:id" component={baseDashboard(VisualizarAluno)} />


        </Switch>
    );
}

// import { VisualizarRl } from '../pages/Role/Visualizar';
//<CustomRoute isPrivate exact path="/visualizarRl/:id" component={VisualizarRl} />