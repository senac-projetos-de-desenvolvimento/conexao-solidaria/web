import React from 'react';
import { Link } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);


const Sidebar = () => (
    <nav className="sidebar">
        <ul className="list-unstyled">
            <li><Link to="/dashboard"> Dashboard</Link></li>
            <li><Link to="/listar"><FontAwesomeIcon icon="users" /> Usuários</Link></li>
            <li><Link to="/listarRl"><FontAwesomeIcon icon="users" /> Níveis de Acesso</Link></li>
            <li><Link to="/listar-cursos"><FontAwesomeIcon icon="church" /> Cursos</Link></li>
            <li><Link to="/listar-instrutores"><FontAwesomeIcon icon="user-graduate" /> Instrutores</Link></li>
            <li><Link to="/listar-participantes"><FontAwesomeIcon icon="users-cog" /> Estudantes</Link></li>
            <li><Link to="/listar-turmas"><FontAwesomeIcon icon="user-friends" /> Turmas</Link></li>
            <li><Link to="/"> Sair</Link></li>
        </ul>
    </nav>
)

export default Sidebar;