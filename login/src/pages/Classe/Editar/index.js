import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo, Form, Label, Input, ButtonWarning } from '../../../styles/custom_adm';
import api from '../../../config/configApi';

export const Editar = (props) => {

    const [id] = useState(props.match.params.id);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    const editUser = async e => {
        e.preventDefault();

        const headers = {
            'Content-Type': 'application/json'
        }

        await api.put("/user", {id, name, email, password}, {headers})
        .then((response) => {
            if(response.data.erro){
                setStatus({
                    type: 'erro',
                    mensagem: response.data.mensagem
                });
            }else{
                setStatus({
                    type: 'success',
                    mensagem: response.data.mensagem
                });
            }
        }).catch(() => {
            setStatus({
                type: 'erro',
                mensagem: 'Erro: Tente mais tarde!'
            });
        });
    }

    useEffect(() => {
        const getUser = async () => {
            await api.get("/user/" + id)
                .then((response) => {
                    if (response.data.erro) {
                        setStatus({
                            type: 'erro',
                            mensagem: response.data.mensagem
                        });
                    } else {
                        setName(response.data.user.name);
                        setEmail(response.data.user.email);
                    }
                })
                .catch(() => {
                    setStatus({
                        type: 'erro',
                        mensagem: "Erro: Tente mais tarde!"
                    });
                });
        }

        getUser();
    }, [id]);

    return (
        <Container>

           

            <ConteudoTitulo>
                <Titulo>Editar Usuário</Titulo>
                <BotaoAcao>
                    <Link to="/listar">
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>

            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}

                <Form onSubmit={editUser}>
                    <Label>Nome: </Label>
                    <Input type="text" name="name" placeholder="Nome do usuário" value={name} onChange={e => setName(e.target.value)} />

                    <Label>E-mail: </Label>
                    <Input type="email" name="email" placeholder="E-mail do usuário" value={email} onChange={e => setEmail(e.target.value)} />

                    <Label>Senha: </Label>
                    <Input type="password" name="password" placeholder="Senha para acessar o administrativo" autoComplete="on"  onChange={e => setPassword(e.target.value)} />

                    <ButtonWarning type="submit">Editar</ButtonWarning>
                </Form>
            </Conteudo>
        </Container>
    )
}