import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo } from '../../../styles/custom_adm';
import { ConteudoUsuario } from './styles';

import api from '../../../config/configApi';

export const Visualizar = (props) => {

    const [data, setData] = useState([]);
    const [id] = useState(props.match.params.id);

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    useEffect(() => {
        const getUser = async () => {
            await api.get("/user/" + id)
                .then((response) => {
                    if (response.data.erro) {
                        setStatus({
                            type: 'erro',
                            mensagem: response.data.mensagem
                        });
                    } else {
                        setData(response.data.user);
                    }
                })
                .catch(() => {
                    setStatus({
                        type: 'erro',
                        mensagem: "Erro: Tente mais tarde!"
                    });
                });
        }

        getUser();
    }, [id]);

    return (
        <Container>

            <ConteudoTitulo>
                <Titulo>Visualizar Usuário</Titulo>
                <BotaoAcao>
                    <Link to="/listar">
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>

            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}

                <ConteudoUsuario>ID: {data.id}</ConteudoUsuario>
                <ConteudoUsuario>Nome: {data.name}</ConteudoUsuario>
                <ConteudoUsuario>E-mail: {data.email}</ConteudoUsuario>

                <ConteudoUsuario>{data.phone !== null ? "Telefone: " + data.phone : ""}</ConteudoUsuario>
            </Conteudo>
        </Container>
    )
}