import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { Container, ConteudoTitulo, Titulo, BotaoAcao, AlertDanger, AlertSuccess, ButtonInfo, Conteudo } from '../../../styles/custom_adm';
import { ConteudoCurso } from './styles';

import api from '../../../config/configApi';

export const VisualizarRl = (props) => {

    const [data, setData] = useState([]);
    const [id] = useState(props.match.params.id);

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });

    useEffect(() => {
        const getRl = async () => {
            await api.get("/role/" + id)
                .then((response) => {
                    if (response.data.erro) {
                        setStatus({
                            type: 'erro',
                            mensagem: response.data.mensagem
                        });
                    } else {
                        setData(response.data.role);
                    }
                })
                .catch(() => {
                    setStatus({
                        type: 'erro',
                        mensagem: "Erro: Tente mais tarde!"
                    });
                });
        }

        getRl();
    }, [id]);

    return (
        <Container>

            <ConteudoTitulo>
                <Titulo>Visualizar Curso</Titulo>
                <BotaoAcao>
                    <Link to="/listar-cursos">
                        <ButtonInfo>Listar</ButtonInfo>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>

            <Conteudo>
                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}

                <ConteudoCurso>ID: {data.id}</ConteudoCurso>
                <ConteudoCurso>Nome: {data.name}</ConteudoCurso>
                <ConteudoCurso>Descricao: {data.description}</ConteudoCurso>
                <ConteudoCurso>Carga Horária: {data.ch}</ConteudoCurso>
                <ConteudoCurso>Data de inicio: {data.start_date}</ConteudoCurso>
                <ConteudoCurso>Data de término: {data.final_date}</ConteudoCurso>

            </Conteudo>
        </Container>
    )
}